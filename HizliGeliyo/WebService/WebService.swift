//
//  WebService.swift
//  HizliGeliyo
//
//  Created by Orhun Dündar on 10.09.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import Alamofire
import ObjectMapper
import SwiftyJSON
import AlamofireObjectMapper

class WebService {
    
    //Check internet connection
    public static var isReachable:Bool {
        get { return NetworkReachabilityManager()?.isReachable ?? false }
    }
    
    //Get product list method
    public static func getProductList(success: @escaping ([ProductModel]?) -> Void){
        
        if isReachable {
            let URL = "https://fakestoreapi.com/products"
            Alamofire.request(URL).responseArray { (response: DataResponse<[ProductModel]>) in
                
                if response.result.isSuccess {
                    
                    let forecastArray = response.result.value
                    if let forecastArray = forecastArray {
                        success(forecastArray)
                    }
                    else {
                        //Error
                        success(nil)
                    }
                }
                else {
                    //Error
                    success(nil)
                }
            }
        }
        else {
            //No internet
            success(nil)
        }
        
        
        
    }
    
    
    
    
}

