//
//  Cart.swift
//  HizliGeliyo
//
//  Created by Orhun Dündar on 15.09.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import Foundation

class Cart {
    
    static let sharedInstance = Cart()
    var productList:[ProductModel] = []
    
    var productCount:Int {
        return productList.count
    }
    
    var sumPrice:Double {
        
        var sum = 0.0
        
        for product in productList {
            sum = sum + Double(Float(product.price))
        }
        
        return sum
    }
    
    func addToChart(product:ProductModel) {
        productList.append(product)
    }
    
    func getAllAddedProductToChart() -> [ProductModel] {
        
        return self.productList
    }
    
    
    
}
