//
//  ProductModel.swift
//  HizliGeliyo
//
//  Created by Orhun Dündar on 10.09.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import ObjectMapper

class ProductModel : Mappable {
    var id:String!
    var title:String!
    var price:Float!
    var description:String!
    var category:String!
    var image:String!
    
    required public init?(map: Map) {
    }
    
    public func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        price <- map["price"]
        description <- map["description"]
        category <- map["category"]
        image <- map["image"]
    }
}

public struct GetProductListResult : Mappable {
    var products:[ProductModel] = []
    
    public init?(map: Map) {
    }
    
    public mutating func mapping(map: Map) {
        products <- map["products"]
    }
}
