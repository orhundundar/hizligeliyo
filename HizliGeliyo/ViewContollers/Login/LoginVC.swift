//
//  LoginVC.swift
//  HizliGeliyo
//
//  Created by Orhun Dündar on 10.09.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import UIKit

class LoginVC: UIViewController {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
    }
    
    //For bottom line
    override func viewDidAppear(_ animated: Bool) {
        self.view.addBottomColoredLine(width:6)
    }
    
    private func initViews() {
                
        titleLabel.font = UIFont(name: "TitilliumWeb-SemiBold", size: 24)
        titleLabel.textColor = UIColor.mainColor200
        
        emailTextField.makeShadow()
        emailTextField.placeholder = "E-mail"
        emailTextField.font = UIFont(name: "TitilliumWeb-Regular", size: 16)
        
        passwordTextField.makeShadow()
        passwordTextField.placeholder = "Şifre"
        passwordTextField.textContentType = .password
        passwordTextField.font = UIFont(name: "TitilliumWeb-Regular", size: 16)
        
        forgotPasswordButton.tintColor = UIColor.mainColor200
        forgotPasswordButton.titleLabel?.font = UIFont(name: "TitilliumWeb-Regular", size: 16)!
        
        loginButton.backgroundColor = UIColor.blue2
        loginButton.tintColor = .white
        loginButton.layer.cornerRadius = 28
        loginButton.titleLabel?.font = UIFont(name: "TitilliumWeb-SemiBold", size: 16)
        
        registerButton.titleLabel?.font = UIFont(name: "TitilliumWeb-Regular", size: 16)
        let att = NSMutableAttributedString(string: "Hesabın yok mu? Kaydol");
        att.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.mainColor200, range: NSRange(location: 0, length: 15))
        att.addAttribute(NSAttributedString.Key.foregroundColor, value: UIColor.blue2, range: NSRange(location: 16, length: 6))
        registerButton.setAttributedTitle(att, for: .normal)
        
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        showEmptyView(title: "Giriş Sayfası")
    }
    
    @IBAction func forgotPasswordPressed(_ sender: Any) {
        showEmptyView(title: "Şifremi Unuttum")
    }
    
    @IBAction func loginButtonPressed(_ sender: Any) {
        let vc = UIStoryboard.init(name: "ProductList", bundle: Bundle.main).instantiateViewController(withIdentifier: "MainTabBarVC")
        UIApplication.shared.keyWindow?.rootViewController = vc
    }
    
    @IBAction func registerButtonPressed(_ sender: Any) {
        showEmptyView(title: "Kaydol")
    }
    
    //Show empty view for missing designs
    func showEmptyView(title:String) {
        let detailView: EmptyViewController = EmptyViewController(nibName: "EmptyViewController", bundle: nil)
        detailView.pageTitle = title
        self.navigationController?.pushViewController(detailView, animated: true)
    }
    

}

extension LoginVC {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
