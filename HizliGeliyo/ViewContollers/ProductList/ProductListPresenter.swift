//
//  ProductListPresenter.swift
//  HizliGeliyo
//
//  Created by Orhun Dündar on 11.09.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import Foundation

protocol ProductListProtocol {
    func fetchedListData(listData:[ProductModel]?)
}

class ProductListPresenter {
    
    var delegate:ProductListProtocol?
    var listData:[ProductModel] = []
    
    init(delegate:ProductListVC) {
        self.delegate = delegate
    }
    
    //Get product list
    public func getDatas(){
        WebService.getProductList { (productList) in
            self.listData = productList!
            self.delegate?.fetchedListData(listData: productList!)
        }
    }
    
    //Get searched products
    public func getSearcedDatas(text:String) {
        let filteredList = listData.filter {
            $0.title.lowercased().range(of: text, options: .caseInsensitive) != nil
        }
        delegate?.fetchedListData(listData: filteredList)
    }
}
