//
//  MainTabBarVC.swift
//  HizliGeliyo
//
//  Created by Orhun Dündar on 10.09.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import UIKit

class MainTabBarVC: UITabBarController {

    @IBOutlet weak var mainTabBar: CustomTabBar!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
}



//For Tab Bar Height
class CustomTabBar : UITabBar {
    
    @IBInspectable var height: CGFloat = 65.0
    
    override open func sizeThatFits(_ size: CGSize) -> CGSize {
        guard let window = UIApplication.shared.keyWindow else {
            return super.sizeThatFits(size)
        }
        var sizeThatFits = super.sizeThatFits(size)
        if height > 0.0 {

            if #available(iOS 11.0, *) {
                sizeThatFits.height = height + window.safeAreaInsets.bottom
            } else {
                sizeThatFits.height = height
            }
        }
        return sizeThatFits
    }
    
}
