//
//  CartTableViewCell.swift
//  HizliGeliyo
//
//  Created by Orhun Dündar on 15.09.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import UIKit

class CartTableViewCell: UITableViewCell {

    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productTitleLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        productTitleLabel.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1)
        productTitleLabel.font = UIFont(name: "TitilliumWeb-SemiBold", size: 14)
        
        productPriceLabel.textColor = UIColor.blue2
        productPriceLabel.font = UIFont(name: "TitilliumWeb-SemiBold", size: 20)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setCell(product:ProductModel) {
        
        self.productTitleLabel.text = product.title
        self.productPriceLabel.text = String(product.price) + " TL"
        self.productImageView.sd_setImage(with: URL(string: product.image), placeholderImage: UIImage(named: "placeholder.png"))
        
    }

}
