//
//  ChartVC.swift
//  HizliGeliyo
//
//  Created by Orhun Dündar on 15.09.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import UIKit

class ChartVC: UIViewController {
    
    @IBOutlet weak var TitleLabel: UILabel!
    @IBOutlet weak var cartTableView: UITableView!
    @IBOutlet weak var priceView: UIView!
    @IBOutlet weak var priceSumLabel: UILabel!
    @IBOutlet weak var sumTitleLabel: UILabel!
    
    
    var chartList:[ProductModel]=[]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initViews()
        
        NotificationCenter.default.addObserver(self, selector: #selector(productAddedToCart), name: .productAddedToCart, object: nil)
        
        chartList = Cart.sharedInstance.getAllAddedProductToChart()
        self.cartTableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        chartList = Cart.sharedInstance.getAllAddedProductToChart()
        self.cartTableView.reloadData()
        initSum()
    }
    
    func initViews(){
        TitleLabel.font = UIFont(name: "TitilliumWeb-SemiBold", size: 24)
        TitleLabel.textColor = UIColor.mainColor200
        
        sumTitleLabel.font = UIFont(name: "TitilliumWeb-SemiBold", size: 14)
        sumTitleLabel.textColor = UIColor.gray2
        
        priceSumLabel.textColor = UIColor.blue2
        priceSumLabel.font = UIFont(name: "TitilliumWeb-SemiBold", size: 20)
        
    }
    
    func initSum() {
        priceSumLabel.text = String(format: "%.2f",Cart.sharedInstance.sumPrice)
    }
    
    @objc func productAddedToCart() {
        if let tabItems = tabBarController?.tabBar.items {
            // In this case we want to modify the badge number of the third tab:
            let tabItem = tabItems[2]
            tabItem.badgeValue = String(Cart.sharedInstance.productCount)
        }
    }
    
}

extension ChartVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        chartList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CartTableViewCell", for: indexPath) as! CartTableViewCell
        cell.setCell(product:self.chartList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    
}
