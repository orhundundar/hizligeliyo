//
//  ProductCollectionViewCell.swift
//  HizliGeliyo
//
//  Created by Orhun Dündar on 10.09.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import UIKit
import SDWebImage

class ProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productTitleLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var productOldPriceLabel: UILabel!
    
    override func layoutSubviews() {
        productImageView.clipsToBounds = true
        productImageView.layer.cornerRadius = 10
        productImageView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
        productTitleLabel.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1)
        productTitleLabel.font = UIFont(name: "TitilliumWeb-SemiBold", size: 16)
        
        productPriceLabel.textColor = UIColor(red: 0.176, green: 0.612, blue: 0.859, alpha: 1)
        productPriceLabel.font = UIFont(name: "TitilliumWeb-SemiBold", size: 18)
        
        productOldPriceLabel.textColor = UIColor(red: 0.554, green: 0.554, blue: 0.554, alpha: 1)
        productOldPriceLabel.font = UIFont(name: "TitilliumWeb-SemiBold", size: 14)
    }
    
    func setCell(product:ProductModel) {
        
        self.productTitleLabel.text = product.title
        self.productPriceLabel.text = String(product.price) + " TL"
        self.productImageView.sd_setImage(with: URL(string: product.image), placeholderImage: UIImage(named: "placeholder.png"))
        
    }
    
}
