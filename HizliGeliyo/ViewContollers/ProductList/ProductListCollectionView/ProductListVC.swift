//
//  ProductListVC.swift
//  HizliGeliyo
//
//  Created by Orhun Dündar on 10.09.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import UIKit

class ProductListVC: UIViewController {
    
    @IBOutlet weak var searchBarTextField: UITextField!
    @IBOutlet weak var productsCollectionView: UICollectionView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var presenter:ProductListPresenter!
    var productList:[ProductModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        productsCollectionView.delegate = self
        initViews()
        
        NotificationCenter.default.addObserver(self, selector: #selector(productAddedToCart), name: .productAddedToCart, object: nil)
        
        //Set presenter
        presenter = ProductListPresenter(delegate:self)
        presenter.getDatas()
        activityIndicator.startAnimating()
        
    }
    
    //Add line to textField
    override func viewDidAppear(_ animated: Bool) {
        searchBarTextField.addBottomColoredLine(width:3)
    }
    
    func initViews(){
        
        //TabBar Indicator
        let tabBar = self.tabBarController!.tabBar
        tabBar.selectionIndicatorImage = UIImage().createSelectionIndicator(color: UIColor.tabBarSelectedColor, size: CGSize(width: tabBar.frame.width/CGFloat(tabBar.items!.count), height: tabBar.frame.height))
        
        //Set Serach Text Field
        searchBarTextField.makeSearchView()
        searchBarTextField.placeholder = "Kategori veya ürün ara"
        searchBarTextField.font = UIFont(name: "TitilliumWeb-SemiBold", size: 16)
        
        activityIndicator.hidesWhenStopped = true
        
    }
    
    @objc func productAddedToCart() {
        if let tabItems = tabBarController?.tabBar.items {
            // In this case we want to modify the badge number of the third tab:
            let tabItem = tabItems[2]
            tabItem.badgeValue = String(Cart.sharedInstance.productCount)
        }
    }
    
}

extension ProductListVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return productList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionViewCell", for: indexPath) as! ProductCollectionViewCell
        cell.setCell(product: productList[indexPath.row])
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detailView: ProductDetailVC = ProductDetailVC(nibName: "ProductDetailVC", bundle: nil)
        detailView.product = self.productList[indexPath.row]
        self.navigationController?.pushViewController(detailView, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let width = (self.view.frame.width - 60) / 2
        let height = (width * 224 )/179
        
        return CGSize(width: width, height: height)
    }
    
}

extension ProductListVC : ProductListProtocol {
    
    func fetchedListData(listData: [ProductModel]?) {
        
        self.productList = listData!
        productsCollectionView.reloadData()
        
        activityIndicator.stopAnimating()
    }
    
    
}

extension ProductListVC : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == searchBarTextField {
            if textField.text?.isEmpty == false {
                activityIndicator.startAnimating()
                activityIndicator.isHidden = false
                presenter.getSearcedDatas(text: textField.text!)
            }
            else {
                presenter.getDatas()
                activityIndicator.startAnimating()
            }
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == searchBarTextField {
            if textField.text?.isEmpty == true {
                presenter.getDatas()
                activityIndicator.startAnimating()
                activityIndicator.isHidden = false
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
}
