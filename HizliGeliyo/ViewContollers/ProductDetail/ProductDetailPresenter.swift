//
//  ProductDetailPresenter.swift
//  HizliGeliyo
//
//  Created by Orhun Dündar on 15.09.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import Foundation

protocol ProductDetailProtocol {
    func productAddedToCart()
}

class ProductDetailPresenter {
    
    var delegate:ProductDetailProtocol?
    
    init(delegate:ProductDetailProtocol) {
        self.delegate = delegate
    }
    
    func addProductToCart(product:ProductModel){
        Cart.sharedInstance.addToChart(product: product)
        delegate?.productAddedToCart()
        NotificationCenter.default.post(name: .productAddedToCart, object: nil, userInfo: nil)
    }
    
}
