//
//  ProductDetailVC.swift
//  HizliGeliyo
//
//  Created by Orhun Dündar on 15.09.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import UIKit
import SDWebImage

class ProductDetailVC: UIViewController {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var productTitleLabel: UILabel!
    @IBOutlet weak var productDescLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    @IBOutlet weak var addToCartButton: UIButton!
    
    var presenter:ProductDetailPresenter!
    var product:ProductModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter = ProductDetailPresenter(delegate: self)
        setViews()
        setProductInfos()
    }
    
    func setViews(){
        titleLabel.text = "Ürün Detayı"
        
        productTitleLabel.textColor = UIColor(red: 0, green: 0, blue: 0, alpha: 1)
        productTitleLabel.font = UIFont(name: "TitilliumWeb-SemiBold", size: 16)
        
        productDescLabel.font = UIFont(name: "TitilliumWeb-SemiBold", size: 14)
        productDescLabel.textColor = UIColor.gray2
        
        productPriceLabel.textColor = UIColor(red: 0.176, green: 0.612, blue: 0.859, alpha: 1)
        productPriceLabel.font = UIFont(name: "TitilliumWeb-SemiBold", size: 24)
        
        
        addToCartButton.backgroundColor = UIColor.blue2
        addToCartButton.tintColor = .white
        addToCartButton.layer.cornerRadius = addToCartButton.frame.height/2
        addToCartButton.titleLabel?.font = UIFont(name: "TitilliumWeb-SemiBold", size: 16)
    }
    
    func setProductInfos(){
        self.productTitleLabel.text = product.title
        self.productDescLabel.text = product.description
        self.productPriceLabel.text = String(product.price) + " TL"
        self.productImageView.sd_setImage(with: URL(string: product.image), placeholderImage: UIImage(named: "placeholder.png"))
        
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func addToCartPressed(_ sender: Any) {
        presenter.addProductToCart(product: product)
    }
    
}

extension ProductDetailVC : ProductDetailProtocol {
    func productAddedToCart() {
        
    }
    
    
}
