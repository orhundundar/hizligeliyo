//
//  EmptyViewController.swift
//  HizliGeliyo
//
//  Created by Orhun Dündar on 11.09.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import UIKit

class EmptyViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    var pageTitle : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = pageTitle
    }
    
    @IBAction func backButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
