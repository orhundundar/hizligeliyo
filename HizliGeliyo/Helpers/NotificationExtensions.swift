//
//  NotificationExtensions.swift
//  HizliGeliyo
//
//  Created by Orhun Dündar on 15.09.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import Foundation

///Notification Names
extension  Notification.Name {
    static let productAddedToCart = Notification.Name(rawValue: "productAddedToChat")
}
