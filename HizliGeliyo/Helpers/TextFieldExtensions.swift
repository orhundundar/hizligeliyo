//
//  TextFieldExtensions.swift
//  HizliGeliyo
//
//  Created by Orhun Dündar on 10.09.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import UIKit

extension UITextField {
    
    func makeShadow(){
        self.layer.cornerRadius = self.frame.height / 2
        self.layer.masksToBounds = false
        self.backgroundColor = .white
        self.textAlignment = .center
        
        self.layer.shadowColor = UIColor(red: 0.253, green: 0.46, blue: 0.805, alpha: 0.08).cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 20)
        self.layer.shadowOpacity = 1
        self.layer.shadowRadius = 20 //Here your control your blur
        
    }
    
    func makeSearchView() {
        let iconView = UIImageView(frame: CGRect(x: 16, y: 16, width: 16, height: 16))
        iconView.image = UIImage(named: "searchIcon")!
        let iconContainerView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 48, height: 45))
        iconContainerView.addSubview(iconView)
        leftView = iconContainerView
        leftViewMode = .always
        self.tintColor = .lightGray
        
        self.layer.cornerRadius = 10
        self.layer.masksToBounds = false
        self.backgroundColor = .white
        self.textColor = UIColor(red: 0.613, green: 0.618, blue: 0.727, alpha: 1)        
        
        self.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.08).cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 6)
        self.layer.shadowOpacity = 1
        self.layer.shadowRadius = 15 //Here your control your blur
    }
    
}
