//
//  UIViewExtensions.swift
//  HizliGeliyo
//
//  Created by Orhun Dündar on 10.09.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import UIKit

extension UIView {
    
    func addBottomColoredLine(width:CGFloat){
        
        let color1 = UIView(frame: CGRect(x: 0, y: self.frame.height - width, width: self.frame.width / 6, height: width))
        color1.backgroundColor = UIColor(red: 0.153, green: 0.682, blue: 0.376, alpha: 1)
        self.addSubview(color1)
        
        let color2 = UIView(frame: CGRect(x: (self.frame.width / 6), y: self.frame.height - width, width: self.frame.width / 6, height: width))
        color2.backgroundColor = UIColor(red: 0.184, green: 0.502, blue: 0.929, alpha: 1)
        self.addSubview(color2)
        
        let color3 = UIView(frame: CGRect(x: (self.frame.width / 6) * 2, y: self.frame.height - width, width: self.frame.width / 6, height: width))
        color3.backgroundColor = UIColor(red: 0.949, green: 0.6, blue: 0.29, alpha: 1)
        self.addSubview(color3)
        
        let color4 = UIView(frame: CGRect(x: (self.frame.width / 6) * 3, y: self.frame.height - width, width: self.frame.width / 6, height: width))
        color4.backgroundColor = UIColor(red: 0.922, green: 0.341, blue: 0.341, alpha: 1)
        self.addSubview(color4)
        
        let color5 = UIView(frame: CGRect(x: (self.frame.width / 6) * 4, y: self.frame.height - width, width: self.frame.width / 6, height: width))
        color5.backgroundColor = UIColor(red: 0.733, green: 0.42, blue: 0.851, alpha: 1)
        self.addSubview(color5)
        
        let color6 = UIView(frame: CGRect(x: (self.frame.width / 6) * 5, y: self.frame.height - width, width: self.frame.width / 6, height: width))
        color6.backgroundColor = UIColor(red: 0.337, green: 0.8, blue: 0.949, alpha: 1)
        self.addSubview(color6)
                
    }
    
    
}


