//
//  Colors.swift
//  HizliGeliyo
//
//  Created by Orhun Dündar on 10.09.2020.
//  Copyright © 2020 Orhun Dündar. All rights reserved.
//

import UIKit

extension UIColor {
    
    @nonobjc class var  mainColor200: UIColor {
        return UIColor(hexString:"#52565C")
    }
    
    @nonobjc class var  gray2: UIColor {
        return UIColor(hexString:"#8D8D8D")
    }
    
    @nonobjc class var  blue2: UIColor {
        return UIColor(hexString:"#2D9CDB")
    }
    
    @nonobjc class var  tabBarSelectedColor: UIColor {
        return UIColor(red: 1, green: 0.852, blue: 0.326, alpha: 1)
    }
    
    @nonobjc class var  tabBarNormalColor: UIColor {
        return UIColor(red: 0.321, green: 0.337, blue: 0.362, alpha: 1)
    }
    
    
    
}

extension UIColor {
    
    convenience init(hexString: String) {
        let hex = hexString.trimmingCharacters(in: CharacterSet.alphanumerics.inverted)
        var int = UInt32()
        Scanner(string: hex).scanHexInt32(&int)
        let a, r, g, b: UInt32
        switch hex.count {
        case 3: // RGB (12-bit)
            (a, r, g, b) = (255, (int >> 8) * 17, (int >> 4 & 0xF) * 17, (int & 0xF) * 17)
        case 6: // RGB (24-bit)
            (a, r, g, b) = (255, int >> 16, int >> 8 & 0xFF, int & 0xFF)
        case 8: // ARGB (32-bit)
            (a, r, g, b) = (int >> 24, int >> 16 & 0xFF, int >> 8 & 0xFF, int & 0xFF)
        default:
            (a, r, g, b) = (255, 0, 0, 0)
        }
        self.init(red: CGFloat(r) / 255, green: CGFloat(g) / 255, blue: CGFloat(b) / 255, alpha: CGFloat(a) / 255)
    }
    
}

